const { error } = require('console');
const http = require('http');
const uuid = require('crypto');
const { port } = require('./config');

http.createServer((request, response) => {
  if (request.method==="GET" && request.url === '/html') {
    response.setHeader('Content-Type',"text/html");
    response.write(`<!DOCTYPE html>
        <html>
          <head>
          </head>
          <body>
              <h1>Any fool can write code that a computer can understand. Good programmers write code that humans can understand.</h1>
              <p> - Martin Fowler</p>
        
          </body>
        </html>`);
    response.end();
  }
  else if (request.method==="GET" && request.url === '/json') {
    response.setHeader('Content-Type',"application/json");
    response.write(`
      {
          "slideshow": {
            "author": "Yours Truly",
            "date": "date of publication",
            "slides": [
              {
                "title": "Wake up to WonderWidgets!",
                "type": "all"
              },
              {
                "items": [
                  "Why <em>WonderWidgets</em> are great",
                  "Who <em>buys</em> WonderWidgets"
                ],
                "title": "Overview",
                "type": "all"
              }
            ],
            "title": "Sample Slide Show"
          }
        }`);
    response.end();
  }
  else if (request.method==="GET" && request.url === '/uuid') {
    response.setHeader('Content-Type',"application/json");
    response.end(JSON.stringify({
      'uuid': uuid.randomUUID()
    }));
  }
  else if (request.method==="GET" && request.url.includes('/status')) {
    let status = request.url.split('/')[2];
    const statusCode = http.STATUS_CODES[status];
    if (statusCode === undefined || status === undefined || status == 100) {
      response.statusCode = 404;
      response.setHeader('Content-Type',"application/json");
      response.end(JSON.stringify(`Check Your Status Code : Invalid`));
    }
    else {
      response.statusCode = parseInt(status);
      response.setHeader('Content-Type',"application/json");
      response.end(JSON.stringify(`Status code : ${status}`));
    }
  }
  else if (request.method === "GET" && request.url.includes('/delay')) {
    const time = request.url.split('/')[2];
    if (time > 0) {
      setTimeout(() => {
        response.setHeader('Content-Type',"application/json");
        response.end(JSON.stringify(`200 : OK`));
      }, time * 1000);
    }
    else {
      response.setHeader('Content-Type',"application/json");
      response.end(JSON.stringify(`Invalid delay time`));
    }
  } else {
    response.setHeader('Content-Type',"text/html");
    response.end(`
    <!DOCTYPE html>
        <html>
          <head>
          </head>
          <body></body><h1>Welcome to Home Page</h1>
    <h3>try these valid EndPoint :-</h3>
    <h4>/html</h4>
    <h4>/json</h4>
    <h4>/status/statusCode</h4>
    <h4>/delay/timeInSecond</h4>
    </body>
      </html>`);
  }
}).listen(port, () => {
  console.log(`server is running ${port}`);
});